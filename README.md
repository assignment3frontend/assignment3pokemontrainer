# Assignment 3 - Angular Pokemon Trainer

Third assignment in the frontend part of the Noroff accelerate course.

## Description

### Part one - Component Tree
In the first part of this assignment we made a component tree to easier see how the application would be built up and to easier see the components each view should have.

### Part two - HTML, CSS & Angular
In the second part of this assignment we build a Angular app with a login page, pokemon-catalogue page and a trainer profile page. The login page only takes a trainer name and based on wether or not the username exists in the given API that we also hosted it creates a user or logs into a user. In the pokemon list part you can catch pokemons by clicking the pokeball in the corner, when the button is pressed it will also post the pokemon to the API. We also created a trainer profile page where you can view "Bill's PC" (your caught pokemon), with the ability to log out and release pokemons by clicking the same pokeball in the corner.

## Project status
All of the functionality is implemented, scalability on mobile devices could be improved and the files with redundant code can be cleaned up.

## Usage
The project can either be downloaded from this repository and run in vscode with the right dependencies installed (npm install). The project is also hosted on Netlify with a build version during grading [Pokemon-Trainer](https://erikogandreaspokedex.netlify.app/) [![Netlify Status](https://api.netlify.com/api/v1/badges/3f8a0bc6-4839-4126-bb4b-06d556265ba9/deploy-status)](https://app.netlify.com/sites/erikogandreaspokedex/deploys)

## Technologies
* HTML
* CSS
* JavaScript
* TypeScript
* Angular
* [PokeApi](https://pokeapi.co/)
* Railway
* Netlify

## Component tree of the Pokemon Trainer application
![component tree](https://gitlab.com/assignment3frontend/assignment3pokemontrainer/uploads/b6b6531df59091af9f4956005a09a02e/Component_tree_3.png)

## Contributors
* Andreas Kvernhaug
* Erik Aardal

# Available scripts:

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 15.2.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

