import { Injectable } from '@angular/core';
import { StorageKeys } from '../enums/storage-keys.enum';
import { PokemonDisplay } from '../models/pokemonDisplay.model';
import { Trainer } from '../models/trainer.model';
import { StorageUtil } from '../utils/storage.utils';

@Injectable({
  providedIn: 'root'
})

export class TrainerService {

  private _trainer?: Trainer;

  get trainer(): Trainer | undefined {
    return this._trainer;
  }
  set trainer(trainer: Trainer | undefined){
    StorageUtil.storageSave<Trainer>(StorageKeys.Trainer, trainer!)
    this._trainer = trainer;
  }

  constructor() {
    this._trainer = StorageUtil.storageRead<Trainer>(StorageKeys.Trainer);
   }

   public catchPokemon(pokemonName: string): void {
    if (this._trainer) {
      this._trainer.pokemon.push(pokemonName)
    }
   }

   public releasePokemon(pokemonName: string): void {
    if (this._trainer) {
      this._trainer.pokemon = this._trainer.pokemon.filter(item => item !== pokemonName)
    }
   }

   public inPokemon(pokemonName: string): boolean {
    if (this._trainer) {
      return Boolean(this._trainer.pokemon.some(pokemon => pokemon === pokemonName))
    }
    return false;
   }
/*
   public inPokemon(pokemonName: string): boolean {
    if (this._trainer) {
      const pokemonArray = this._trainer.pokemon
      console.log(Boolean(this.trainer?.pokemon.find((pokemon: PokemonDisplay) => pokemon.name === pokemonName)))
      if(pokemonArray.some(pokemon => pokemon.name === pokemonName)) {
        return true;
      }
      else {
        return false
      }
     //return Boolean(this.trainer?.pokemon.find((pokemon: PokemonDto) => pokemon.name === pokemonName))
    }
    return false;
   }
*/
  
}
