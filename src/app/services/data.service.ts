import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { PokemonDto } from '../models/pokemonDto.model';

const { apiPokemons } = environment;

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private _pokemons: PokemonDto[] = [];
  private _error: string = "";

  get pokemons(): PokemonDto[] {
    return this._pokemons;
  }
  
  get error(): string {
    return this._error;
  }

  constructor(
    private readonly http: HttpClient
  ) { }

  public findAllPokemons(): void {
    this.http.get<PokemonDto[]>(apiPokemons)
    .subscribe({
      next: (pokemons: PokemonDto[]) => {
        this._pokemons = pokemons;
      },
      error: (error: HttpErrorResponse) => {
        this._error = error.message;
      }
    })
  }

  public pokemonByName(name: string): PokemonDto | undefined {
    return this._pokemons.find((pokemon: PokemonDto) => pokemon.name === name)
  }

  getPokemons(limit: number){
    return this.http.get(`https://pokeapi.co/api/v2/pokemon?limit=${limit}`);
  }

  getPokemon(name: string){
    return this.http.get(`https://pokeapi.co/api/v2/pokemon/${name}`)
  }

}
