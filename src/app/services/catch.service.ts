import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalize, Observable, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Trainer } from '../models/trainer.model';
import { DataService } from './data.service';
import { TrainerService } from './trainer.service';

const { apiUsersKey, apiUsers } = environment;

@Injectable({
  providedIn: 'root'
})
export class CatchService {

  private _loading: boolean = false;

  get loading(): boolean {
    return this._loading;
  }

  constructor(
    private http: HttpClient,
    private readonly dataService: DataService,
    private readonly trainerService: TrainerService
  ) { }

  public addPokemonToTrainer(name: string): Observable<Trainer> {

    if (!this.trainerService.trainer) {
      throw new Error("addPokemonToTrainer: There is no user");
    }
    const trainer: Trainer = this.trainerService.trainer;

    const pokemon = this.dataService.getPokemon(name);

    if(!pokemon) {
      throw new Error("addPokemonToTrainer: No pokemon with name: " + name)
    }

    if(this.trainerService.trainer?.pokemon.some(pokemon => pokemon === name)) {
      this.trainerService.releasePokemon(name);
      //throw new Error("addPokemonToTrainer: Pokemon already in list")
    } else{
      this.trainerService.catchPokemon(name);
    }
      
    const headers = new HttpHeaders({
      'content-type': 'application/json',
      'x-api-key': apiUsersKey
    })

    this._loading = true;

    return this.http.patch<Trainer>(`${apiUsers}/${trainer.id}`, {
      pokemon: [...trainer.pokemon]
    }, {
      headers
    })
    .pipe(
      tap((updatedTrainer: Trainer) => {
        this.trainerService.trainer = updatedTrainer;
      }),
      finalize(() => {
        this._loading = false;
      })
    )
  }
}
