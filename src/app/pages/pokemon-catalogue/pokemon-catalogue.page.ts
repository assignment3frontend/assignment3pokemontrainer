import { Component, OnInit } from '@angular/core';
import { PokemonDto } from 'src/app/models/pokemonDto.model';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-pokemon-catalogue',
  templateUrl: './pokemon-catalogue.page.html',
  styleUrls: ['./pokemon-catalogue.page.css']
})
export class PokemonCataloguePage implements OnInit {

  get pokemons(): PokemonDto[] {
    return this.dataService.pokemons;
  }

  get error(): string {
    return this.dataService.error;
  }

  constructor (
    private readonly dataService: DataService
  ) {}

  ngOnInit(): void {
    this.dataService.findAllPokemons();
  }

}
