import { Component, Input, OnInit } from '@angular/core';
import { Trainer } from 'src/app/models/trainer.model';
import { DataService } from 'src/app/services/data.service';
import { LogoutService } from 'src/app/services/logout.service';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.page.html',
  styleUrls: ['./trainer.page.css']
})
export class TrainerPage implements OnInit{
  pokemonDisplay: any[] = [];

  @Input() pokemonName: string = "";

  get trainer(): Trainer | undefined {
    return this.trainerService.trainer
  }

  get pokemons(): string[] {
    if (this.trainerService.trainer) {
      return this.trainerService.trainer.pokemon
    }
    return [];
  }

  constructor (
    private logoutService: LogoutService,
    private trainerService: TrainerService,
    private dataService: DataService
  ) {}

  ngOnInit(): void {
    this.getPokemons();
  }

  logout() {
    if(window.confirm("Are you sure?")) {
      this.logoutService.logout()
      window.location.reload();
    }
  }

  getPokemons() {
    if(this.trainerService.trainer) {
      this.trainerService.trainer.pokemon.forEach((name: string) => {
        this.dataService.getPokemon(name).subscribe((uniqueResponse: any) => {
          this.pokemonDisplay.push(uniqueResponse);
        });
      });
    }
  }

}
