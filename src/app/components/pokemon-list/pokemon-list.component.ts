import { Component, Input, OnInit } from '@angular/core';
import { PokemonDto } from 'src/app/models/pokemonDto.model';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.css']
})
export class PokemonListComponent implements OnInit{
  pokemonDisplay: any[] = [];
  show = false;

  @Input() pokemons: PokemonDto[] = [];

  constructor(
    private dataService: DataService
  ) {}

  ngOnInit(): void {
    this.getPokemons();
  }

  showMore() {
    this.show = true;
  }
  showLess() {
    this.show = false;
  }

  getPokemons() {
    this.dataService.getPokemons(151).subscribe((response: any) => {
      response.results.forEach((result: { name: string; }) => {
        this.dataService.getPokemon(result.name).subscribe((uniqueResponse: any) => {
          this.pokemonDisplay.push(uniqueResponse);
        });
      });
    });
  }



}
