import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { Trainer } from 'src/app/models/trainer.model';
import { CatchService } from 'src/app/services/catch.service';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
  selector: 'app-catch-pokemon',
  templateUrl: './catch-pokemon.component.html',
  styleUrls: ['./catch-pokemon.component.css']
})
export class CatchPokemonComponent implements OnInit {

  public isCatched: boolean = false;

  @Input() pokemonName: string = "";

  get loading(): boolean {
    return this.catchService.loading;
  }

  constructor (
    private trainerService: TrainerService,
    private readonly catchService: CatchService
  ) {}

  ngOnInit(): void {
      this.isCatched = Boolean(this.trainerService.trainer?.pokemon.some(pokemon => pokemon === this.pokemonName))
  }

  onCatchClick(): void {
    this.catchService.addPokemonToTrainer(this.pokemonName)
      .subscribe({
        next: (response: Trainer) => {
          this.isCatched = this.trainerService.inPokemon(this.pokemonName)
        },
        error: (error: HttpErrorResponse) => {
          console.log("Error", error.message);
        }
      })
  }
}
