import { PokemonDisplay } from "./pokemonDisplay.model";

export interface Trainer {
    id: number;
    username: string;
    pokemon: string[];
}